import readline
import atexit
import os

python_history = os.environ.get('XDG_CACHE_HOME', None)
if python_history:
    python_history = os.path.join(python_history, 'python_history')
else:
    python_history = os.path.join(os.environ['HOME'], '.python_history')

try:
    readline.read_history_file(python_history)
    readline.set_history_length(1000)
except FileNotFoundError:
    open(python_history, 'wb').close()

atexit.unregister(readline.write_history_file)
atexit.register(readline.write_history_file, python_history)
